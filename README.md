# France_Inter

## Consignes

L’équipe sécurité de Radio France (RF) vous demande de réaliser une solution technologique assurant la distanciation sociale dans leurs studios d’enregistrement.

RF utilise une caméra IP dans chacun des studios d’enregistrement et vous met à disposition une image issue de la caméra toutes les X secondes.

Vous devez écrire un programme prenant en entrée l’image pour y détecter le respect des règles de distanciation. Si la distanciation n’est pas respectée, un message audio en français et en anglais doit être diffusé.

Le cahier des charges indique :

les studios étant de taille réduite, seul X personnes en simultané sont admises pour considérer que la distanciation est respectée. Ce nombre doit être paramétrable depuis le backoffice.
le message audio doit être diffusé en français et en anglais
le message audio diffusé doit être entré sur un backoffice sous forme d’une chaîne de caractères. L’application doit automatiquement générer le message audio à partir du texte.
Attention : l’équipe RF qui administrera l’application étant francophone, le message doit être inscrit dans le backoffice uniquement en français.
L’application se chargera automatiquement de le traduire en anglais avant la diffusion.

Ce projet comporte 2 étapes pouvant être développées en parallèle.

## Etape 1 

Réaliser une interface web ou client lourd (backoffice) permettant à l’opérateur d’entrer le message qui sera diffusé sous forme d’une chaîne de caractère. Une fois le message entré et traité par votre application, les messages audio en français et anglais sont sauvegardés dans des fichiers.

Exemple de message : « X personnes maximums dans le studio, merci de respecter les distances de sécurité »).

L’opérateur doit aussi pouvoir sélectionner le nombre de personnes admises en simultanées dans le studio depuis le backoffice. Ce nombre est sauvegardé dans un fichier de configuration (ex : format JSON)

## Etape 2

Réaliser un script prenant en entrée les fichiers suivants :

l’image d’une des caméras
le fichier de configuration contenant le nombre de personnes admises (en bonus, vous pouvez ajouter des options supplémentaires)
le fichier audio en français
le fichier audio en anglais
Si le script détecte que la distanciation sociale est respectée, il s'arrête.

Si le script détecte que la distanciation sociale n’est pas respectée, il diffuse les messages audio puis s'arrête.

## Informations

Pour réaliser cette application, les langages sont libres. Vous devrez cependant utiliser les services proposés par Microsoft Azure.

Des images du studio sont disponibles en ressource sur l’intra.
